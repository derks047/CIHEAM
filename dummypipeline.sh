#!/bin/bash
# qsub/slurm/scheduler options can go here
# Define ref
REF=ref.fa
# Path to known SNPS
DBSNP=/path/to/DB/SNP.vcf
# Path to GATK jar file
GenomeAnalysisTK=/path/to/GATK/GenomeAnalysisTK.jar
# Define stub name, take as variable 
STUB=$1
# trim data
sickle pe -f $STUB-R1.fq.gz -r $STUB-R2.fq.gz -o $STUB-R1.fq.tr -p $STUB-R2.fq.tr -s $STUB.singles.tr -l 45 -t sanger
pigz $STUB-R1.fq.tr; pigz $STUB-R2.fq.tr

# maping using the bwa-aln algorithm, including sorting of bam
bwa aln -n 0.07 -t 4 ref.fa $STUB-R1.fq.tr.gz  >$STUB-R1.fq.tr.sai
bwa-0.7.5a/bwa aln -n 0.07 -t 4 $REF $STUB-R2.fq.tr.gz  >$STUB-R2.fq.tr.sai
bwa sampe -P $REF -r '@RG\tID:id\tSM:id\tPL:ILLUMINA' $STUB-R1.fq.tr.sai $STUB-R2.fq.tr.sai $STUB-R1.fq.tr.gz $STUB-R2.fq.tr.gz | samtools view -q 20 -Suh - | samtools sort -m 5000000000 - $STUB
samtools index $STUB.bam

# dedup using samtools
samtools rmdup $STUB.bam $STUB.dedup.bam
samtools index $STUB.dedup.bam

# re-alignment using GATK-RealignmentTargetCreator+IndelRealigner
java -jar $GenomeAnalysisTK -nt 4 -T RealignerTargetCreator -R $REF -I $STUB.dedup.bam -o $STUB.dedup.reA.intervals
java -jar $GenomeAnalysisTK -T IndelRealigner -R ref.fa -I $STUB.dedup.bam -targetIntervals $STUB.dedup.reA.intervals -o $STUB.dedup.reA.bam
ln -s $STUB.dedup.reA.bai $STUB.dedup.reA.bam.bai

# Do some reporting to log file
DATE=`date`; echo 'produced BAM file '$STUB'.dedup.reA.bam: '$DATE  >>$STUB.log
FSIZE=`stat --printf="%s" $STUB.dedup.reA.bam`; echo "size of file "$STUB".dedup_st.reA.bam is "$FSIZE  >>$STUB.log
MD5BAM=`md5sum $STUB.dedup.reA.bam | sed 's/ \+/	/' | cut -f1`; echo "md5sum of file "$STUB".dedup.reA.bam is "$MD5BAM  >>$STUB.log

# Variant calling using GATK UnifiedGenotyper
java -jar $GenomeAnalysisTK -nt 4 -R ref.fa -T UnifiedGenotyper -I $STUB.dedup.reA.bam --dbsnp $DBSNP --genotype_likelihoods_model BOTH -o $STUB.vcf -stand_call_conf 50.0 -stand_emit_conf 10.0  -dcov 200
